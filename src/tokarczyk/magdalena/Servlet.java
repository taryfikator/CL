package tokarczyk.magdalena;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/hello")
public class Servlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("<html><body><h2> ClassLoadery wykorzystane podczas dzia\\u0142ania prostej aplikacji: </h2></body></html>");
        printCLs(Servlet.class.getClassLoader(), out);
        out.close();

    }

    private void printCLs(ClassLoader classLoader, PrintWriter pw) {
        if(classLoader != null) {
            printCLs(classLoader.getParent(), pw);
        }
        pw.println("<html><body><h2>ClassLoader: " + classLoader + "</h2></body></html>");
    }

}
